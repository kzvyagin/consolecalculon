#include <QCoreApplication>
#include "calculon.h"
#include <QDebug>
#include <QStringList>
#include <stdexcept>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    if( a.arguments().value(1).size()==0 || a.arguments().value(1).contains("--help"))
    {
        qDebug()<<"Console calculator.";
        qDebug()<<"Availible commands:";
        qDebug()<<"sub,add,mult,div,let";
        qDebug()<<"Usage: add(1,2) ; sub(4,5) ; let(a,5,sub(2,a))";
        qDebug()<<"Author: Zvyagin Konstantin k.n.zvyagin@yandex.ru";
        return 0;
    }

    qDebug()<<Q_FUNC_INFO<<"Input string:"<<a.arguments().value(1);
    double result=0;
    try
    {
        result=Calculon().process(a.arguments().value(1));
    }
    catch (const std::invalid_argument& e)
    {
        qDebug()<<Q_FUNC_INFO<<"Exeption:"<<QByteArray(e.what());
        return -1;
    }
    qDebug()<<Q_FUNC_INFO<<"result:"<<result;
    return 0;
}
