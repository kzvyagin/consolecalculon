#include "calculon.h"
#include <QDebug>
#include "calculon_p.h"

Calculon::Calculon():d( new PrivateCalculon() )
{

}

Calculon::~Calculon()
{
    delete d;
}

double Calculon::process(const QString &string)
{
    if( d->process(string) )
        return d->result;
    return Functor::nan;
}



Calculon::Calculon(const Calculon &root)
{
    d=new PrivateCalculon;
    *d=*root.d;

}

Calculon &Calculon::operator=(const Calculon &other)
{
    d=new PrivateCalculon();
    *d=*other.d;
    return *this;
}

