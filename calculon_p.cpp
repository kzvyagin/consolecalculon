#include "calculon_p.h"
#include <QRegExp>
#include <QDebug>
#include <QStringList>
#include "hlf.h"
#include <cstdint>

PrivateCalculon::PrivateCalculon():result(Functor::nan)
{

}

bool PrivateCalculon::process(const QString &strings)
{
    if( parse(strings) )
    {
        result=calculate();
        return true;
    }
    throw std::invalid_argument("Can't parse string, brackests not consistent!");
    return false;
}

bool PrivateCalculon::parse(const QString &string)
{
    source=string;

    int depthOfNesting=0;

    for(int index=0;index<string.size(); )
    {
        Functor f;
        if( string.midRef(index,4)=="mult")
        {
            f.type=Functor::MULT;
            f.functorPosition=index;
            index+=4;
        }
        else if (string.midRef(index,3)=="add")
        {
            f.type=Functor::ADD;
            f.functorPosition=index;
            index+=3;

        }
        else if (string.midRef(index,3)=="sub")
        {
            f.type=Functor::SUB;
            f.functorPosition=index;
            index+=3;

        }
        else if (string.midRef(index,3)=="div")
        {
            f.type=Functor::DIV;
            f.functorPosition=index;
            index+=3;

        }
        else if (string.midRef(index,3)=="let")
        {
            f.type=Functor::LET;
            f.functorPosition=index;
            index+=3;

        } else if( string[index]==QChar(')') )
        {
            --depthOfNesting;
            ++index;
            continue;
        }
        else if( string[index]==QChar('(') )
        {
            ++depthOfNesting;
            ++index;
            continue;
        }else
        {
            ++index;
            continue;
        }


        if(f.isFunctor())
        {
            f.depthOfNesting=depthOfNesting;
            f.openBracketPosition  = findOpenBracketPosition(string,
                                                             f.functorPosition);
            f.closeBracketPosition = findCloseBracketPosition(string,
                                                              f.openBracketPosition);

            functors<<f;
        }

    }

    if( depthOfNesting==0 )
        return true;

    return false;
}

bool lessThanByDepthOfNesting(const Functor &f1, const Functor &f2)
{
    return f1.depthOfNesting < f2.depthOfNesting;
}

double PrivateCalculon::calculate( )
{
    qSort(functors.begin(),functors.end(),lessThanByDepthOfNesting);

    double result=Functor::nan;

    if(functors.size() >0 )
    {
        Functor &f=functors[0];
        f.setEquationiResult( processFunctor( f ) );
        result=f.equationResult();
        if(std::isnan(result))
            throw std::invalid_argument("Can't calculate equation, got nan!");
        if(std::isinf(result))
            throw std::invalid_argument("Can't calculate equation, got inf!");
    }
    else
    {
         throw std::invalid_argument((QString("Can't calculate equation, no functors! Equation:%1").arg(source)).toUtf8().data());
    }


    return result;
}

struct allWithSameDepthLevel
{
    int depthLevel;
    allWithSameDepthLevel(int depthLevel):depthLevel(depthLevel){}
    bool operator()(const Functor&f) { return f.depthOfNesting==depthLevel;}
};

struct findMinDepthLevel
{
    int &depthLevel;
    findMinDepthLevel(int &depthLevel):depthLevel(depthLevel){}
    void operator()(const Functor&f) {
        if(f.depthOfNesting < depthLevel )
            depthLevel=f.depthOfNesting;
    }
};

bool lessThanByPosition(const Functor &f1, const Functor &f2)
{
    return f1.functorPosition < f2.functorPosition;
}

double PrivateCalculon::processFunctor( Functor &f )
{

    QString fString=getFunctorString(f);
    QStringList list=separateByComma(fString);
    QList<Functor> variables=getInsideOperators( f  );
    int extDepthLevel=INT_MAX;
    std::for_each(variables.begin(),variables.end(), findMinDepthLevel(extDepthLevel) );
    variables=hlf::filter(variables,allWithSameDepthLevel(extDepthLevel));
    qSort( variables.begin(), variables.end(), lessThanByDepthOfNesting);

    if( f.type==Functor::LET )
    {
        QString operatorName=list.value(0).trimmed();
        if( isPureDiginsValue( list.value(1) )  && variables.size() == 1 )
        {
            globalVariables.insert(operatorName,list.value(1).toDouble( ));
            double result=processFunctor( variables[0] );
            return result;
        }else if( variables.size() == 2 )
        {
            double val=processFunctor( variables[0] );
            globalVariables.insert(operatorName,val);
            double result=processFunctor( variables[1] );
            return result;
        }

    }



    // case when we have simple equation like add(1,5) or div(a,b)
    if( variables.size()==0 )
    {
        bool ok_a=false;
        bool ok_b=false;
        QString strA=list.value(0).trimmed();
        QString strB=list.value(1).trimmed();

        double a=strA.toDouble(&ok_a);
        double b=strB.toDouble(&ok_b);

        if( !isPureDiginsValue(strA)  )
        {
            if( !globalVariables.contains(strA))
                throw std::invalid_argument( (QString("No variable with name \"%1\" in memory!").arg(strA)).toUtf8().data());
            a=globalVariables.value(strA);
        }

        if( !isPureDiginsValue(strB)  )
        {
             if( !globalVariables.contains(strB) )
                 throw std::invalid_argument( (QString("No variable with name \"%1\" in memory!").arg(strA)).toUtf8().data());
            b=globalVariables.value(strB);
        }

        //qDebug()<<Q_FUNC_INFO<<"strA="<<strA<<"strB="<<strB;
        f.setEquationiResult(  f.makeCalculations(a,b) );
        return f.equationResult();
    }


    // case when we have sub equation like add(1, div(a,b)) or add(div(2,2), div(a,b))
    QList<double> results;
    for( int i=0; i<variables.size(); i++ )
    {
        if( !variables[i].isResultAllreadyReady() )
        {
            results<<processFunctor( variables[i] );
        }
        else
        {
            results<<variables[i].equationResult();
        }
    }

    foreach (auto result, results) {
        if(std::isnan(result))
            throw std::invalid_argument((QString("Can't calculate equation, got nan! Equation:%1").arg(fString)).toUtf8().data());
        if(std::isinf(result))
            throw std::invalid_argument((QString("Can't calculate equation, got inf!  Equation:%2").arg(fString)).toUtf8().data());
    }

    if( results.size() == 2 )
    {
        f.setEquationiResult( f.makeCalculations(results.value(0),results.value(1)) );
        return f.equationResult();
    }



    int index=getPureDigitIndex(list);

    if( index==-1 )
        return f.equationResult();


    double value = list.value(index).toDouble();
    double a=value;
    double b=results.value(0);
    if(index==1)
    {
        a=results.value(0);
        b=value;
    }


    f.setEquationiResult( f.makeCalculations(a,b) );
    return f.equationResult();
}

int PrivateCalculon::findOpenBracketPosition(const QString &string,
                                             int functorPosition)
{
    for( int i=functorPosition; i<string.size() ; ++i)
    {
        if( string[i]==QChar('(') )
        {
            return i;
        }
    }
    return -1;
}

int PrivateCalculon::findCloseBracketPosition(const QString &string,
                                              int openBracketPosition)
{
    int depthOfNesting=1;
    for( int i=openBracketPosition+1; i<string.size() ; ++i)
    {
        //qDebug()<<Q_FUNC_INFO<< string.mid(i,1);
        if( string[i]==QChar(')') )
        {
            --depthOfNesting;
        }
        else if( string[i]==QChar('(') )
        {
            ++depthOfNesting;
        }

        if(depthOfNesting==0)
            return i;
    }
    return -1;
}

QStringList PrivateCalculon::separateByComma(const QString &str)
{
    QStringList list;

    int depthOfNesting=0;
    QList<int> commaPositions;
    for( int i=0; i<str.size() ; ++i)
    {
        if( str[i]==QChar(')') )
        {
            --depthOfNesting;
            continue;
        }
        else if( str[i]==QChar('(') )
        {
            ++depthOfNesting;
            continue;
        }

        if( str[i] == QChar(',')  && depthOfNesting==0 )
        {
            commaPositions<<i;
        }
    }

    int prevCommaPosition=0;

    foreach (int commaPosition, commaPositions)
    {
        int spanLength=commaPosition-prevCommaPosition;
        list<<str.mid(prevCommaPosition, spanLength );
        prevCommaPosition=commaPosition+1;
    }

    list<<str.mid(prevCommaPosition);
    //qDebug()<<Q_FUNC_INFO<<commaPositions<<list;
    return list;
}

bool PrivateCalculon::isPureDiginsValue(const QString str)
{

    foreach (auto ch, str)
    {
        if(!ch.isDigit() && !ch.isSpace() && ch!=QChar('.') && ch!=QChar('-'))
        {
            return false;
        }
    }
    return true;

}

int PrivateCalculon::getPureDigitIndex(const QStringList &pair)
{
    if( isPureDiginsValue( pair.value(0) ) )
        return 0;
    if( isPureDiginsValue( pair.value(1) ) )
        return 1;
    return -1;
}

QString PrivateCalculon::getFunctorString(const Functor &f)
{
    return source.mid( f.openBracketPosition+1, f.closeBracketPosition-f.openBracketPosition-1 );
}

struct filterAllInsideRootFunctor
{
    const Functor &root;

    filterAllInsideRootFunctor(const Functor &root):root(root){}

    bool operator()(const Functor &item)
    {
        if(root.functorPosition == item.functorPosition)
            return false;
        if(    item.functorPosition < root.functorPosition
               || item.functorPosition >root.closeBracketPosition )
            return false;

        return true;
    }
};

QList<Functor> PrivateCalculon::getInsideOperators(const Functor &item)
{
    QList<Functor> insideOperators = hlf::filter(functors, filterAllInsideRootFunctor(item) );
    return insideOperators;
}

Functor::Functor(): type(NONE),
    functorPosition(-1),
    openBracketPosition(-1),
    closeBracketPosition(-1),
    depthOfNesting(-1),
    _resultCalculated(false),
    _equationResult(nan)
{
}

bool Functor::isBracketsCosisten() const
{
    return     openBracketPosition !=-1
            && closeBracketPosition!=-1;
}

bool Functor::isFunctor()
{
    return     type!=NONE
            && functorPosition!=-1;
}

double Functor::makeCalculations(double a, double b)
{
    double result=Functor::nan;
    switch (  type )
    {
    case MULT:
        result=a*b;
        break;
    case ADD:
        result=a+b;
        break;
    case SUB:
        result=a-b;
        break;
    case DIV:
        result=a/b;
        break;
    default:
        break;
    }
    return result;
}

void Functor::setEquationiResult(double value)
{
    _resultCalculated=true;
    _equationResult=value;
}

double Functor::equationResult()
{
    return _equationResult;
}

bool Functor::isResultAllreadyReady()
{
    return _resultCalculated;
}


