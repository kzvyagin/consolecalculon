#ifndef HIGH_LEVEL_FUNCTIONS_H
#define HIGH_LEVEL_FUNCTIONS_H

#include <algorithm>
#include <numeric>

///
/// Hight level functions
///
namespace hlf {
    template< typename Container, typename _Predicate >
    Container filter( const Container & conatiner , _Predicate predicate )
    {
        Container result;
        for (const auto& val : conatiner)
        {
            if ( predicate( val ) )
            {
                 result << val;
            }
        }
        return result;
    }


    template< typename Container, typename _Predicate >
    Container map( const Container & conatiner , _Predicate predicate )
    {
        Container result;
        std::transform(conatiner.begin(), conatiner.end(), result, predicate);
        return result;
    }

    template< typename Container, typename _ResultContainer, typename _Predicate >
    void map( const Container & conatiner, _ResultContainer & result , _Predicate predicate )
    {
        _ResultContainer _result;
        for(int i=0;i<conatiner.size();i++){
            _result << predicate( conatiner[i] );
        }
        result=_result;
    }

    template< typename Container, typename _ResultContainer, typename _Predicate >
    _ResultContainer map( const Container & conatiner, _Predicate predicate )
    {
        _ResultContainer _result;
        for(int i=0;i<conatiner.size();i++){
            _result << predicate( conatiner[i] );
        }
        return _result;
    }


    template< typename Container,  typename _elementType , typename _Predicate >
    _elementType reduce( const Container & conatiner ,_elementType initval, _Predicate predicate )
    {
        return std::accumulate(conatiner.begin(), conatiner.end(), initval, predicate);
    }

}

#endif // HIGH_LEVEL_FUNCTIONS_H
