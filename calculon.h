#ifndef CALCULON_H
#define CALCULON_H
#include <QString>

class PrivateCalculon;
class Calculon
{
public:
    Calculon();
    ~Calculon();
    Calculon(const Calculon& root);
    Calculon& operator=(const Calculon& other);

public:
    double process(const QString & string);
protected:

    PrivateCalculon *d;

};

#endif // CALCULON_H
