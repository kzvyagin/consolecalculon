#include <QString>
#include <QtTest>
#include "../calculon.h"
#include "ccalculontestimpl.h"

class CCaclulonTest : public QObject
{
    Q_OBJECT

public:
    CCaclulonTest();

private Q_SLOTS:
    void GeneralTest_data();
    void GeneralTest();
};

CCaclulonTest::CCaclulonTest()
{
}

void CCaclulonTest::GeneralTest_data()
{
    QTest::addColumn<QString>("inputString");
    QTest::addColumn<int>("countOFfunctors");
    QTest::addColumn<bool>("bracketsConsistent");
    QTest::addColumn<double>("answer");

     QTest::newRow("6") << QString("let(a, 5, let(b, mult(a, 10), add(b, a)))")<<4<<true<<55.;

     QTest::newRow("5") << QString("let(a, 5, add(a, a))")<<2<<true<<10.;


     QTest::newRow("3") << QString("add(1, mult(2, 3))")<<2<<true<<7.;
     QTest::newRow("1") << QString("mult(2, 2)")<<1<<true<<4.;
     QTest::newRow("2") << QString("add(1, 2)")<<1<<true<<3.;

     QTest::newRow("4") << QString("mult(add(2, 2), div(9, 3))")<<3<<true<<12.;

     QTest::newRow("7") << QString("let(a, let(b, 10, add(b, b)), let(b, 20, add(a, b)))")<<5<<true<<40.;
}

void CCaclulonTest::GeneralTest()
{
    QFETCH(QString, inputString);
    QFETCH(int,  countOFfunctors);
    QFETCH(bool, bracketsConsistent);
    QFETCH(double, answer);

    qDebug()<<Q_FUNC_INFO<<"\n++++++++++++++++BEGIN++++++++++++++++";
    qDebug()<<Q_FUNC_INFO<<"Test data:"<<inputString;
    CCalculonTestImpl c;

    double result=c.process(inputString);

    QVERIFY2( ( c.getFunctorsCount()==countOFfunctors ) ,
               (  QString("Functions doesn't detected correct! ")+
                  QString("Expected: %1,recived:%2 ").arg(countOFfunctors).arg(c.getFunctorsCount())
                ).toUtf8().data() );

    qDebug()<<c.isBracketsConsistent()<< bracketsConsistent;
    QVERIFY2( (c.isBracketsConsistent() == bracketsConsistent ) ,
                (  QString("Brackets consistent is broken! ")
                ).toUtf8().data());


    QVERIFY2( (std::isnan(result) && std::isnan(answer))||(result==answer), (  (
                                       QString("Result not the same! Expected: %1 got: %2 ").arg(answer).arg(result)
                                   )
                                   ).toUtf8().data());
}

QTEST_APPLESS_MAIN(CCaclulonTest)

#include "tst_ccaclulontest.moc"
