#include "ccalculontestimpl.h"
#include "../calculon_p.h"
CCalculonTestImpl::CCalculonTestImpl():Calculon()
{

}

int CCalculonTestImpl::getFunctorsCount()
{
    return d->functors.size();
}

bool CCalculonTestImpl::isBracketsConsistent()
{
    foreach (  const Functor& f, d->functors)
    {
        if(!f.isBracketsCosisten())
            return false;
    }
    return true;
}

