#ifndef CCALCULONTESTIMPL_H
#define CCALCULONTESTIMPL_H
#include "../calculon.h"
class PrivateCalculon;
class CCalculonTestImpl:public Calculon
{
public:
    CCalculonTestImpl();
    int getFunctorsCount();
    bool isBracketsConsistent();
public:
     int size();
};

#endif // CCALCULONTESTIMPL_H
