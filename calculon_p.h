#ifndef CALCULON_P_H
#define CALCULON_P_H
#include <QList>
#include <QStringRef>
#include <QHash>


struct Functor
{
    static constexpr double nan = std::numeric_limits< double >::quiet_NaN();
    enum Type{
        NONE=0,
        MULT,
        ADD,
        SUB,
        DIV,
        LET
    };

    Functor();
    bool isBracketsCosisten() const;

    bool isFunctor();
    double makeCalculations(double a,double b);


    Type type;
    int functorPosition;
    int openBracketPosition;
    int closeBracketPosition;
    int depthOfNesting;

    void setEquationiResult(double value);
    double equationResult();
    bool isResultAllreadyReady();
private:
    bool   _resultCalculated;
    double _equationResult;


};

class PrivateCalculon
{

public:
    PrivateCalculon();
    bool process(const QString & strings);
    bool parse(const QString & string);
    double calculate();
    double processFunctor(Functor &f);
    int  findOpenBracketPosition(const QString &string,
                                 int functorPosition);
    int findCloseBracketPosition(const QString &string,
                                  int openBracketPosition);

    QStringList separateByComma(const QString & str);

    bool isPureDiginsValue(const QString str);
    int getPureDigitIndex(const QStringList &pair );
    QString getFunctorString(const Functor &f);
    QList<Functor> getInsideOperators(const Functor &item);
    QString source;
    QList<Functor> functors;
    QHash<QString,double> globalVariables;
    double result;
};


#endif // CALCULON_P_H
